package control;

/**
 * Created by thyago on 13/11/15.
 */
public class EntityNotFoundException extends Exception {
    public EntityNotFoundException(){
        super("Nome não foi encontrado.\n");
    }
    public EntityNotFoundException(String message){
        super(message);
    }
}
