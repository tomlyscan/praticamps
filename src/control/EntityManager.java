package control;

import memento.EmptyStatesListException;
import memento.Entity;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by thyago on 13/11/15.
 */
public class EntityManager {
    private List<Entity> listEntity;

    public EntityManager(){
        listEntity = new ArrayList<>();
    }

    public void addEntity(String name){
        Entity newEntity = new Entity(name);
        listEntity.add(newEntity);
    }

    public void updateEntity(String oldName,String newName) throws EntityNotFoundException {
        boolean found = false;
        for(Entity e : listEntity){
            if(e.getEntity().equals(oldName)){
                e.writeName(newName);
                found = true;
            }
        }if(!found) throw new EntityNotFoundException();
    }

    public List listEntities(){
        return listEntity;
    }

    public void undoEntity(String name) throws EmptyStatesListException, EntityNotFoundException{
        boolean found = false;
        for(Entity e : listEntity){
            if(e.getEntity().equals(name)){
                e.undoName();
                found = true;
            }
        }if(!found) throw new EntityNotFoundException();
    }

    public void removeEntity(String name) throws EntityNotFoundException {
        boolean found = false;
        for(Entity e : listEntity){
            if(e.getEntity().equals(name)){
                listEntity.remove(e);
                found = true;
            }
        }if(!found) throw new EntityNotFoundException();
    }
}
