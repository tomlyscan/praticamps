package memento;

import java.util.ArrayList;

/**
 * Created by thyago on 13/11/15.
 */
public class EntityCareTaker {
    protected ArrayList<EntityMemento> states;

    public EntityCareTaker(){
        states = new ArrayList<EntityMemento>();
    }

    public void addMemento(EntityMemento memento){
        states.add(memento);
    }

    public EntityMemento getLastSaveState() throws EmptyStatesListException {
        if(states.size() <= 0)
            throw new EmptyStatesListException();
        EntityMemento saveState = states.get(states.size() -1);
        states.remove(states.size() -1);
        return saveState;
    }
}
