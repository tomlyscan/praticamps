package memento;

/**
 * Created by thyago on 13/11/15.
 */
public class EmptyStatesListException extends Exception {

    public EmptyStatesListException(){
        super("A lista de estados está vazia!\n");
    }

    public EmptyStatesListException(String message){
        super(message);
    }
}
