package memento;

/**
 * Created by thyago on 13/11/15.
 */
public class EntityMemento {
    protected String entityState;

    public EntityMemento(String name){
        entityState = name;
    }

    public String getSaveEntity(){
        return entityState;
    }
}
