package memento;

/**
 * Created by thyago on 13/11/15.
 */
public class Entity {
    protected String name;
    EntityCareTaker caretaker;

    public Entity(){
        caretaker = new EntityCareTaker();
        name = new String();
    }

    public Entity(String name){
        caretaker = new EntityCareTaker();
        this.name = name;
    }

    public void writeName(String newName){
        caretaker.addMemento(new EntityMemento(newName));
    }

    public void undoName() throws EmptyStatesListException {
        name = caretaker.getLastSaveState().getSaveEntity();
    }

    public String getEntity(){
        return name;
    }
}
