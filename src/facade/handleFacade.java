package facade;

import command.AddCommand;
import command.Command;
import command.ListCommand;
import command.UpdateCommand;
import control.EntityManager;

import java.util.HashMap;
import java.util.List;

/**
 * Created by thyago on 13/11/15.
 */
public class handleFacade {

    private static EntityManager instance;
    private HashMap commands = new HashMap();

    private void initCommands() {
        commands.put("new",new AddCommand());
        commands.put("list",new ListCommand());
        commands.put("update",new UpdateCommand());
    }

    public static EntityManager getInstance(){
        if(instance == null)
            instance = new EntityManager();
        return instance;
    }

    public void add(String command,String name){
        Command c = (Command)commands.get(command);
        c.execute(name);
    }

    public void update(String command,String oldName,String newName){
        Command c = (Command)commands.get(command);
        c.execute(oldName,newName);
    }

    public List list(String command){
        Command c = (Command)commands.get(command);
        return (List)c.execute();
    }
}