package command;

import control.EntityManager;
import facade.handleFacade;

/**
 * Created by thyago on 13/11/15.
 */
public class AddCommand implements Command {
    private EntityManager manager;

    public AddCommand(){
        manager = handleFacade.getInstance();
    }
    public Object execute(){return true;}
    public Object execute(Object name) {
        manager.addEntity((String)name);
        return true;
    }
    public Object execute(Object arg1,Object arg2){return true;}
}
