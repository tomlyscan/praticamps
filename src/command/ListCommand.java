package command;

import control.EntityManager;
import facade.handleFacade;

import java.util.List;

/**
 * Created by thyago on 13/11/15.
 */
public class ListCommand implements Command {
    private EntityManager manager;

    public ListCommand(){
        manager = handleFacade.getInstance();
    }

    public List execute() {
        return manager.listEntities();
    }
    public Object execute(Object arg1,Object arg2){ return true; }
    public Object execute(Object arg1){ return true; }

}
