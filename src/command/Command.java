package command;

import java.util.Map;

/**
 * Created by thyago on 13/11/15.
 */
public interface Command {
    Object execute();
    Object execute(Object arg1);
    Object execute(Object arg1,Object arg2);
}
