package command;

import control.EntityManager;
import control.EntityNotFoundException;
import facade.handleFacade;

/**
 * Created by thyago on 13/11/15.
 */
public class UpdateCommand implements Command {
    private EntityManager manager;

    public UpdateCommand(){
        this.manager = handleFacade.getInstance();
    }
    public Object execute(){return true;}
    public Object execute(Object newName) { return true; }
    public Object execute(Object oldName,Object newName) {
        try {
            manager.updateEntity((String) oldName, (String) newName);
        }catch(EntityNotFoundException ex){
            ex.printStackTrace();
        }
        return true;
    }
}